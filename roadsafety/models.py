from __future__ import unicode_literals

from django.contrib.auth.models import User
from django.contrib.gis.db import models

from django.utils import timezone
try:
    from django.utils.encoding import force_text
except ImportError:
    from django.utils.encoding import force_unicode as force_text

from django.utils.encoding import python_2_unicode_compatible

from phonenumber_field.modelfields import PhoneNumberField


class Hospital(models.Model):
    name = models.CharField('Name Of Hospital', max_length=200)
    phone_num = PhoneNumberField(
        "Hospital's Contact Number", blank=True, null=True)
    city = models.CharField('City', max_length=50)
    pincode = models.CharField('Pincode', max_length=6)
    location = models.PointField('Location Of Hospital', blank=True, null=True)
    address = models.TextField('Address', blank=True)

    objects = models.GeoManager()

    def __unicode__(self):
        return self.name


class Ambulence(models.Model):
    hospital = models.ForeignKey(Hospital)
    reg_num = models.CharField('Registration No.', max_length=50, unique=True)
    location = models.PointField('Location Of Ambulence')
    available = models.BooleanField('Available', default=True)

    objects = models.GeoManager()

    def __unicode__(self):
        return self.reg_num


class Profile(models.Model):
    user = models.OneToOneField(User, related_name='profile')
    dob = models.DateField('Date Of Birth', blank=True, null=True)
    phone_num = PhoneNumberField("Contact Number", blank=True, null=True)
    blood_grp = models.CharField('Blood Group', max_length=25, blank=True, null=True)
    pre_med_con = models.TextField('Pre-Medical Condition', blank=True)
    location = models.PointField('Location Of User', blank=True, null=True)
    address = models.TextField('Address', blank=True)
    is_complete = models.BooleanField('Profile Complete', default=False)

    objects = models.GeoManager()

    def __unicode__(self):
        return force_text(self.user.email)
