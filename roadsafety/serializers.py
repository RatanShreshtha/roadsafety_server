from django.contrib.auth.models import User

from rest_framework import serializers
from rest_framework import permissions

from roadsafety.models import *
from roadsafety.permissions import IsOwnerOrReadOnly


class HospitalSerializer(serializers.ModelSerializer):

    class Meta:
        model = Hospital
        fields = '__all__'


class AmbulenceSerializer(serializers.ModelSerializer):

    class Meta:
        model = Ambulence
        fields = '__all__'


class UserSerializer(serializers.ModelSerializer):
    profile = serializers.PrimaryKeyRelatedField(
        queryset=Profile.objects.all())

    class Meta:
        model = User
        fields = '__all__'


class ProfileSerializer(serializers.ModelSerializer):

    user = serializers.ReadOnlyField(source='user.username')
    permission_classes = (
        permissions.IsAuthenticatedOrReadOnly, IsOwnerOrReadOnly)

    class Meta:
        model = Profile
        fields = '__all__'
