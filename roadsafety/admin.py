from django.contrib.gis import admin

from leaflet.admin import LeafletGeoAdmin

from models import *

admin.site.register(Ambulence, LeafletGeoAdmin)
admin.site.register(Hospital, LeafletGeoAdmin)
admin.site.register(Profile, LeafletGeoAdmin)
