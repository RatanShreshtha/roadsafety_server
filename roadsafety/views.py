from django.core.mail import send_mail

from django.contrib.auth.models import User
from django.shortcuts import render

from rest_framework import viewsets
from twilio.rest import	TwilioRestClient

from forms import ContactForm
from models import *
from serializers import *


def index(request):
    return render(request, 'index.html')


# API views for rest_framework

class HospitalViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Hospital.objects.all()
    serializer_class = HospitalSerializer


class AmbulenceViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Ambulence.objects.all()
    serializer_class = AmbulenceSerializer


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = User.objects.all()
    serializer_class = UserSerializer


class ProfileViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Profile.objects.all()
    serializer_class = ProfileSerializer

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


def smsNum(request):
    accountSID = 'AC5e355f4c17ec6cff1ef3493119eabb88'
    authToken = '5d24f4afb37fba754ec3c16ebf0f0ea9'
    client = TwilioRestClient(accountSID, authToken)
    client.messages.create(
    to="+918906413935",
    from_="+16152704072",
    body="An accident at 23.546348, 87.288245.",)
    return render(request, 'index.html')
